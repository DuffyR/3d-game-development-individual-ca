﻿/*
Function: 		Use this class to say exactly how your game listens for events and responds with changes to the game.
Author: 		NMCG
Version:		1.0
Date Updated:	17/11/17
Bugs:			
Fixes:			None
*/

using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace GDApp
{
    public class MyGameStateManager : GameStateManager
    {
        private List<BossCollidablePrimitiveObject> bossList; //Total number of unique bosses in the game
        private Queue<BossCollidablePrimitiveObject> currentBossQueue; // Number of bosses the player must go through
        private bool bBossIsPresent; //Is there a boss (any kind) currently present in the game?
        private int introCameraCountdownInMs; //How long the intro lasts for (Equals the full length of the intro camera's curve)

        private ManagerParameters managerParameters; // A reference to the managers (e.g. Object Manager)
        private MyAppMenuManager menuManager; // A reference to the game's menu

        public MyGameStateManager(Game game, EventDispatcher eventDispatcher, StatusType statusType, MyAppMenuManager menuManager, ManagerParameters managerParameters) 
            : base(game, eventDispatcher, statusType)
        {
            this.menuManager = menuManager;
            this.managerParameters = managerParameters;

            bossList = new List<BossCollidablePrimitiveObject>(7);
            currentBossQueue = new Queue<BossCollidablePrimitiveObject>();
            bBossIsPresent = false;
            introCameraCountdownInMs = 7008; //A bit more than the total time of the intro camera's curve since it's not divisible by 16
        }

        #region Event Handling
        protected override void RegisterForEventHandling(EventDispatcher eventDispatcher)
        {
            eventDispatcher.PlayerChanged += EventDispatcher_PlayerChanged;

            base.RegisterForEventHandling(eventDispatcher);
        }

        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if (eventData.EventType == EventActionType.OnStart)
            {
                this.managerParameters.CameraManager.ActiveCamera.SetAllControllers(PlayStatusType.Play);

                //turn on update and draw i.e. hide the menu
                this.StatusType = StatusType.Update | StatusType.Drawn;
            }
            //did the event come from the main menu and is it a pause game event
            else if (eventData.EventType == EventActionType.OnPause)
            {
                //turn off update and draw i.e. show the menu since the game is paused
                this.StatusType = StatusType.Off;
            }
        }

        private void EventDispatcher_PlayerChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnLose)
            {
                Reset();
                menuManager.SetActiveList("lose menu");
                EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

                object[] additionalParameters = { "game_lost" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            } else if (eventData.EventType == EventActionType.OnWin)
            {
                bBossIsPresent = false;
                string activeMenu = (currentBossQueue.Count == 0) ? "win menu" : "next level menu";
                menuManager.SetActiveList(activeMenu);
                this.managerParameters.ObjectManager.Remove(actor => (actor.GroupParameters != null) && (actor.GroupParameters.UniqueGroupID == AppData.BossProjectileGroupID));

                //Move the player back to the starting position
                PrimitiveObject player = this.managerParameters.ObjectManager.OpaqueDrawList.Find(p => p is PlayerCollidablePrimitiveObject) as PlayerCollidablePrimitiveObject;
                player.Transform.TranslateTo(new Vector3(0, 10, 0));

                EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

                if (activeMenu.Equals("win menu"))
                {
                    object[] additionalParameters = { "game_won" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
        }
        #endregion

        public void AddBoss(BossCollidablePrimitiveObject boss)
        {
            bossList.Add(boss);
            currentBossQueue.Enqueue(boss);
        }

        private void Reset()
        {
            //Remove any bosses that are currently in the game
            BossCollidablePrimitiveObject bossInGame = this.managerParameters.ObjectManager.OpaqueDrawList.Find(b => b is BossCollidablePrimitiveObject) as BossCollidablePrimitiveObject;
            this.managerParameters.ObjectManager.Remove(bossInGame);
            bBossIsPresent = false;

            //Reset the intro camera
            introCameraCountdownInMs = 7008;
            this.managerParameters.CameraManager.SetActiveCamera(c => c.ID.Equals(AppData.IntroCameraID));

            //Move the player back to the starting position
            PrimitiveObject player = this.managerParameters.ObjectManager.OpaqueDrawList.Find(p => p is PlayerCollidablePrimitiveObject) as PlayerCollidablePrimitiveObject;
            player.Transform.TranslateTo(new Vector3(0, 10, 0));

            //Reset the player's lives
            object[] additionalParameters = { "Player Lives Controller", new Integer(5) };
            EventDispatcher.Publish(new EventData(EventActionType.OnHealthSet, EventCategoryType.Player, additionalParameters));

            currentBossQueue.Clear();
            foreach (var boss in bossList)
            {
                boss.Reset();
                currentBossQueue.Enqueue(boss);
            }
        }

        private void SpawnBoss()
        {
            if (currentBossQueue.Count > 0)
            {
                BossCollidablePrimitiveObject currentBoss = currentBossQueue.Dequeue();
                EventDispatcher.Publish(new EventData(currentBoss, EventActionType.OnAddActor, EventCategoryType.SystemAdd));

                object[] additionalParameters = { "Boss Health Controller", new Integer(currentBoss.BossMaxPhases) };
                EventDispatcher.Publish(new EventData(EventActionType.OnMaxHealthSet, EventCategoryType.Player, additionalParameters));

                bBossIsPresent = true;
            }
        }

        protected override void ApplyUpdate(GameTime gameTime)
        {
            if (!bBossIsPresent)
                 SpawnBoss();

            //If the intro camera is active, set its curve controller to be on if it isn't already
            if (this.managerParameters.CameraManager.ActiveCamera.ID.Equals(AppData.IntroCameraID))
            {
                //this.managerParameters.CameraManager.ActiveCamera.SetAllControllers(PlayStatusType.Play);
                introCameraCountdownInMs -= gameTime.ElapsedGameTime.Milliseconds;

                if(this.managerParameters.KeyboardManager.IsFirstKeyPress(Keys.Space))
                {
                    introCameraCountdownInMs = 0;
                }

                if (introCameraCountdownInMs <= 0)
                {
                    this.managerParameters.CameraManager.ActiveCamera.SetAllControllers(PlayStatusType.Reset);
                    object[] additionalEventParamsB = { AppData.InGameCameraID };
                    EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
                }

            }

            base.ApplyUpdate(gameTime);
        }
    }
}
