﻿using GDLibrary;
using System;
using Microsoft.Xna.Framework;

namespace GDApp
{
    public class MySimplePickingManager : SimplePickingManager
    {
        #region Statics
        protected static readonly int DefaultDistanceToTargetPrecision = 2;
        #endregion

        #region Fields
        private int totalElapsedTimeInMs;
        private int minTimeBetweenSoundEventInMs = 250;
        #endregion

        public MySimplePickingManager(Game game, EventDispatcher eventDispatcher, StatusType statusType, ManagerParameters managerParameters) : base(game, eventDispatcher, statusType, managerParameters)
        {
        }

        protected override void HandleResponse(GameTime gameTime, Actor3D collidee)
        {
           
        }
    }
}
