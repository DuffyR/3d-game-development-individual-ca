﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class BossCollidablePrimitiveObject : CollidablePrimitiveObject
    {
        #region Fields
        protected int bossMaxHealth; // How much health the boss is created with
        protected int bossCurrentHealth; // How much health the boss currently has
        protected int bossMaxPhases;
        protected int bossCurrentPhase;
        protected int projectilesFired = 1; //For the IDs of projectiles
        private int bossElapsedTime = 0;
        private int bossLeewayCountdownInMs; //So that the boss doesn't instantly shoot once the game begins

        // Death-related
        private bool bDefeated = false; //For when HP drops to 0, but the boss is not death yet (for death sequences)
        private bool bIsMarkedAsDead = false; //When the boss is officially dead
        private int deathCountdownInMs; // How long the boss' death sequence lasts for

        protected Dictionary<string, ProjectileCollidablePrimitiveObject> bossProjectileDictionary;
        protected ManagerParameters managerParameters;
        #endregion

        #region Properties
        public int BossMaxPhases
        {
            get
            {
                return this.bossMaxPhases;
            }
        }

        public int BossCurrentPhase
        {
            get
            {
                return this.bossCurrentPhase;
            }
        }

        public int BossElapsedTime
        {
            get
            {
                return bossElapsedTime;
            }
        }

        public int BossLeewayCountdownInMs
        {
            get
            {
                return bossLeewayCountdownInMs;
            }
            set
            {
                bossLeewayCountdownInMs = (value >= 0) ? value : 0;
            }
        }

        public bool BDefeated
        {
            get
            {
                return this.bDefeated;
            }
        }

        public bool BIsMarkedAsDead
        {
            get
            {
                return this.bIsMarkedAsDead;
            }
        }
        #endregion

        public BossCollidablePrimitiveObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters,
            StatusType statusType, IVertexData vertexData, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, int bossHealth, int phases)
            : base (id, actorType, transform, effectParameters, statusType, vertexData, collisionPrimitive, managerParameters.ObjectManager)
        {
            Initialize(managerParameters, bossHealth, phases);
        }

        public BossCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, int bossHealth, int phases)
            : base(primitiveObject, collisionPrimitive, managerParameters.ObjectManager)
        {
            Initialize(managerParameters, bossHealth, phases);
        }

        private void Initialize(ManagerParameters managerParameters, int bossHealth, int phases)
        {
            this.bossMaxHealth = bossHealth;
            this.bossCurrentHealth = bossHealth;
            this.bossMaxPhases = phases;
            this.bossCurrentPhase = phases;
            this.managerParameters = managerParameters;

            this.bossLeewayCountdownInMs = 1000;
            this.deathCountdownInMs = 4500;
            bossProjectileDictionary = new Dictionary<string, ProjectileCollidablePrimitiveObject>();
        }

        public void AddNewProjectile(string id, ProjectileCollidablePrimitiveObject projectile)
        {
            bossProjectileDictionary.Add(id, projectile);
        }

        public override void Update(GameTime gameTime)
        {
            if (this.managerParameters.CameraManager.ActiveCamera.ID.Equals("In-Game Camera"))
            {
                if (bDefeated)
                {
                    UpdateDeathState(gameTime);
                } else
                {
                    UpdateActiveState(gameTime);
                }

                ApplyInput(gameTime);

                if (bossLeewayCountdownInMs > 0)
                {
                    bossLeewayCountdownInMs -= gameTime.ElapsedGameTime.Milliseconds;
                } else
                {
                    bossElapsedTime += gameTime.ElapsedGameTime.Milliseconds;
                }

                base.Update(gameTime);
            }
        }

        protected virtual void UpdateActiveState(GameTime gameTime)
        {
            this.Collidee = CheckCollisions(gameTime);

            HandleCollisionResponse(this.Collidee);

            HandleHealth();

            if (bossProjectileDictionary != null && bossLeewayCountdownInMs <= 0)
                PerformAttacks(this.Collidee, gameTime);
        }

        private void UpdateDeathState(GameTime gameTime)
        {
            //Spin the boss
            //Decrease the timer
            //Mark the boss as dead if the timer is up
            this.Transform.RotateIncrement = 10;

            this.deathCountdownInMs -= gameTime.ElapsedGameTime.Milliseconds;

            if (deathCountdownInMs < 2000)
                this.StatusType = StatusType.Update; //No longer drawn

            if (deathCountdownInMs <= 0 && !bIsMarkedAsDead)
            {
                bIsMarkedAsDead = true;

                EventDispatcher.Publish(new EventData(this, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));

                object[] additionalParameters = { "Fred Flintstone" };
                EventDispatcher.Publish(new EventData(EventActionType.OnWin, EventCategoryType.Player, additionalParameters));
            }
        }

        protected override void HandleCollisionResponse(Actor collidee)
        {
            //The boss will only respond to projectiles from the player
            if (collidee is CollidablePrimitiveObject)
            {
                if (collidee.GroupParameters != null)
                {
                    if (collidee.GroupParameters.UniqueGroupID == AppData.PlayerProjectileGroupID)
                    {
                        bossCurrentHealth--;
                        EventDispatcher.Publish(new EventData(collidee, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));

                        object[] additionalParameters = { "boss_damage" };
                        EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                    }
                }
            }
        }

        protected virtual void HandleHealth()
        {
            int healthInterval = this.bossMaxHealth / this.bossMaxPhases;

            if (this.bossCurrentHealth <= healthInterval * (this.bossCurrentPhase - 1))
            {
                this.bossCurrentPhase--;

                object[] additionalParameters = { "Boss Health Controller", new Integer(-1) };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalParameters));
            }

            if (this.bossCurrentHealth == 0)
            {
                object[] additionalParameters = { "boss_defeated" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));

                bDefeated = true;
            }
        }

        protected virtual void PerformAttacks(Actor collidee, GameTime gameTime)
        {
            //To be used by an inherited class
        }

        public virtual void Reset()
        {
            this.StatusType = StatusType.Update | StatusType.Drawn;

            this.bossCurrentHealth = this.bossMaxHealth;
            this.bossCurrentPhase = this.bossMaxPhases;

            this.bossElapsedTime = 0;
            this.bossLeewayCountdownInMs = 1000;
            this.deathCountdownInMs = 4500;

            bIsMarkedAsDead = false;
            bDefeated = false;
        }
    }
}
