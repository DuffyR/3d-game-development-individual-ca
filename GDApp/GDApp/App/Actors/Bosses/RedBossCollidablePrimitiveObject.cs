﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class RedBossCollidablePrimitiveObject : BossCollidablePrimitiveObject
    {
        private bool bIntoPhaseTwo = false;
        private bool bIntoPhaseThree = false;

        private int regularAttackInterval;
        private int phaseThreeAttackInterval;

        public RedBossCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, int bossHealth, int phases, int regularAttackInterval, int phaseThreeAttackInterval)
            : base(primitiveObject, collisionPrimitive, managerParameters, bossHealth, phases)
        {
            this.regularAttackInterval = regularAttackInterval;
            this.phaseThreeAttackInterval = phaseThreeAttackInterval;
        }

        protected override void HandleHealth()
        {
            if (this.bossCurrentPhase <= 2 && !bIntoPhaseTwo)
            {
                this.Transform = new Transform3D(new Vector3(-7.5f, 21, 0), Vector3.Zero, Vector3.One * 3, -Vector3.UnitZ, Vector3.UnitY);
                TranslationSineLerpController bossSidewaysSineLerpController = new TranslationSineLerpController("Boss Translation Lerp", ControllerType.LerpTranslation,
                new Vector3(1, 0, 0), new TrigonometricParameters(15, 0.1f, 180));
                AttachController(bossSidewaysSineLerpController);
                bIntoPhaseTwo = true;
            }

            bIntoPhaseThree = (this.bossCurrentPhase <= 1) ? true : false;

            base.HandleHealth();
        }

        protected override void PerformAttacks(Actor collidee, GameTime gameTime)
        {
            Vector3 direction = Vector3.Zero;
            
            //-- Attack 1: A shot in the direction of where the player was when the attack was called
            if (BossElapsedTime % regularAttackInterval == 0)
            {
                ProjectileCollidablePrimitiveObject shotProjectileObject = this.bossProjectileDictionary["Regular Attack"].Clone() as ProjectileCollidablePrimitiveObject;
                shotProjectileObject.Transform.Translation = this.Transform.Translation;
                shotProjectileObject.Transform.ScaleTo(new Vector3(0.5f));
                shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 0.5f);
                shotProjectileObject.ID = "Red Boss Projectile-" + projectilesFired;
                shotProjectileObject.EffectParameters.DiffuseColor = Color.DarkRed;
                shotProjectileObject.GroupParameters = new GroupParameters("BossProjectile", AppData.BossProjectileGroupID);

                PlayerCollidablePrimitiveObject player = this.ObjectManager.OpaqueDrawList.Find(x => x.ID.Equals("Main Player")) as PlayerCollidablePrimitiveObject;
                direction = MathUtility.GetNormalizedObjectToTargetVector(this.Transform, player.Transform);
                shotProjectileObject.MoveDirection = direction;
                shotProjectileObject.MoveSpeed = 0.012f;

                EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));

                object[] additionalParameters = { "proj_shot" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));

                projectilesFired++;
            }

            //-- Attack 2: A 3-bullet spread straight down (Phase 3 only)
            if (bIntoPhaseThree && (BossElapsedTime + 752) % phaseThreeAttackInterval == 0)
            {
                Matrix[] directionOffsets = { Matrix.CreateRotationZ(MathHelper.ToRadians(-30)),
                    Matrix.CreateRotationZ(MathHelper.ToRadians(0)),
                    Matrix.CreateRotationZ(MathHelper.ToRadians(30))};
                //Vector3[] directionOffsets = { -Vector3.UnitY, new Vector3(0.5f, -0.866f, 0), new Vector3(-0.5f, -0.866f, 0) };

                for (int i = 0; i < directionOffsets.Length; i++)
                {
                    ProjectileCollidablePrimitiveObject shotProjectileObject = this.bossProjectileDictionary["Regular Attack"].Clone() as ProjectileCollidablePrimitiveObject;
                    shotProjectileObject.Transform.Translation = this.Transform.Translation;
                    shotProjectileObject.Transform.ScaleTo(new Vector3(0.4f));
                    shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 0.4f);
                    shotProjectileObject.ID = "Red Boss Phase 3 Projectile-" + projectilesFired;
                    shotProjectileObject.EffectParameters.DiffuseColor = Color.IndianRed;
                    shotProjectileObject.GroupParameters = new GroupParameters("BossProjectile", AppData.BossProjectileGroupID);
                    
                    shotProjectileObject.MoveDirection = Vector3.Transform(-Vector3.UnitY, directionOffsets[i]);
                    shotProjectileObject.MoveSpeed = 0.009f;
                    EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));
                    
                    projectilesFired++;
                }

                object[] additionalParameters = { "proj_shot2" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
        }

        public override void Reset()
        {
            base.Reset();

            //Find the translation lerp controller, and remove it
            this.DetachControllers(c => c is TranslationSineLerpController);

            //Reset the boss' position
            this.Transform = new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, Vector3.One * 3, -Vector3.UnitZ, Vector3.UnitY);

            //Reset the boss' phase flags
            bIntoPhaseTwo = false;
            bIntoPhaseThree = false;
        }
    }
}
