﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class OrangeBossCollidablePrimitiveObject : BossCollidablePrimitiveObject
    {
        private bool bIntoPhaseTwo = false;
        private int phaseTwoShotAngle;

        public OrangeBossCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, int bossHealth, int phases, int phaseTwoShotAngle)
            : base(primitiveObject, collisionPrimitive, managerParameters, bossHealth, phases)
        {
            this.phaseTwoShotAngle = phaseTwoShotAngle;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void UpdateActiveState(GameTime gameTime)
        {
            PlayerCollidablePrimitiveObject player = this.ObjectManager.OpaqueDrawList.Find(x => x.ID.Equals("Main Player")) as PlayerCollidablePrimitiveObject;
            float distance = player.Transform.Translation.X - this.Transform.Translation.X;

            if (distance < -0.2f)
            {
                this.Transform.TranslateIncrement = -Vector3.UnitX * gameTime.ElapsedGameTime.Milliseconds * (player.MoveSpeed * 0.5f);
            }
            else if (distance > 0.2f)
            {
                this.Transform.TranslateIncrement = Vector3.UnitX * gameTime.ElapsedGameTime.Milliseconds * (player.MoveSpeed * 0.5f);
            }

            base.UpdateActiveState(gameTime);
        }

        protected override void HandleHealth()
        {
            bIntoPhaseTwo = (this.bossCurrentPhase <= 2) ? true : false;

            base.HandleHealth();
        }

        protected override void PerformAttacks(Actor collidee, GameTime gameTime)
        {
            //Get a reference to the player
            PlayerCollidablePrimitiveObject player = this.ObjectManager.OpaqueDrawList.Find(x => x.ID.Equals("Main Player")) as PlayerCollidablePrimitiveObject;
            float distance = player.Transform.Translation.X - this.Transform.Translation.X;

            //-- Attack 1: A large projectile straight down if the player is directly below
            if (BossElapsedTime % 1312 == 0)
            {
                ProjectileCollidablePrimitiveObject shotProjectileObject = this.bossProjectileDictionary["Regular Attack"].Clone() as ProjectileCollidablePrimitiveObject;
                shotProjectileObject.Transform.Translation = this.Transform.Translation;
                shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 1);
                shotProjectileObject.ID = "Orange Boss Projectile";
                shotProjectileObject.EffectParameters.DiffuseColor = Color.OrangeRed;
                shotProjectileObject.GroupParameters = new GroupParameters("BossProjectile", AppData.BossProjectileGroupID);
                shotProjectileObject.MoveDirection = -Vector3.UnitY;
                shotProjectileObject.MoveSpeed = 0.022f;

                EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));

                object[] additionalParameters = { "proj_shot2" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }

            //-- Attack 2: A pair of projectiles aimed at the player's sides
            if (bIntoPhaseTwo && BossElapsedTime % 656 == 0)
            {
                //Find the direction towards the player
                Vector3 playerDirection = MathUtility.GetNormalizedObjectToTargetVector(this.Transform, player.Transform);

                //Create the rotations we will apply this vector by
                Matrix[] playerDirectionOffsets = { Matrix.CreateRotationZ(MathHelper.ToRadians(phaseTwoShotAngle)),
                    Matrix.CreateRotationZ(MathHelper.ToRadians(-phaseTwoShotAngle))};

                //Create the projectiles
                for (int i = 0; i < playerDirectionOffsets.Length; i++)
                {
                    ProjectileCollidablePrimitiveObject shotProjectileObject = this.bossProjectileDictionary["Regular Attack"].Clone() as ProjectileCollidablePrimitiveObject;
                    shotProjectileObject.Transform.Translation = this.Transform.Translation;
                    shotProjectileObject.Transform.ScaleTo(new Vector3(0.6f));
                    shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 0.6f);
                    shotProjectileObject.ID = "Orange Boss Phase 2 Projectile";
                    shotProjectileObject.EffectParameters.DiffuseColor = Color.Orange;
                    shotProjectileObject.GroupParameters = new GroupParameters("BossProjectile", AppData.BossProjectileGroupID);

                    shotProjectileObject.MoveDirection = Vector3.Transform(playerDirection, playerDirectionOffsets[i]);
                    shotProjectileObject.MoveSpeed = 0.01f;

                    EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));
                }

                object[] additionalParameters = { "proj_shot" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
        }

        public override void Reset()
        {
            base.Reset();

            bIntoPhaseTwo = false;

            this.Transform.Rotation = Vector3.Zero;
        }
    }
}
