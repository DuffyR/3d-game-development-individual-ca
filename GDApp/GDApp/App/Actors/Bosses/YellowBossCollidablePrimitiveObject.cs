﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class YellowBossCollidablePrimitiveObject : BossCollidablePrimitiveObject
    {
        #region Fields
        private Keys[] moveKeys;

        private int projectileDirection = 1;
        #endregion

        public YellowBossCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, int bossHealth, int phases, Keys[] moveKeys)
            : base(primitiveObject, collisionPrimitive, managerParameters, bossHealth, phases)
        {
            this.moveKeys = moveKeys;
            this.BossLeewayCountdownInMs = 0;
        }

        protected override void PerformAttacks(Actor collidee, GameTime gameTime)
        {
            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(Keys.Space))
            {
                ProjectileCollidablePrimitiveObject shotProjectileObject = GetProjectileFromDictionary();
                shotProjectileObject.Transform.Translation = this.Transform.Translation;
                shotProjectileObject.Transform.ScaleTo(new Vector3(0.5f));
                shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 0.5f);
                shotProjectileObject.MoveDirection = (projectileDirection > 0) ? -Vector3.UnitX : Vector3.UnitX;

                YellowBossProjectileController projectileController = shotProjectileObject.ControllerList.Find(c => c is YellowBossProjectileController) as YellowBossProjectileController;
                projectileController.RotationDirection = (projectileDirection > 0)
                    ? YellowBossProjectileRotationDirection.Counterclockwise : YellowBossProjectileRotationDirection.Clockwise;
                projectileDirection *= -1; //Flip the sign

                EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));

                object[] additionalParameters = { "proj_shot" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
        }

        private ProjectileCollidablePrimitiveObject GetProjectileFromDictionary()
        {
            string dictionaryKey = "";

            if (this.bossCurrentPhase == 1)
            {
                Random r = new Random();
                dictionaryKey = (r.Next(0, 20) > 10) ? "Tracing Attack" : "Tracing Attack Type 2";
            } else
            {
                dictionaryKey = "Tracing Attack";
            }

            return this.bossProjectileDictionary[dictionaryKey].Clone() as ProjectileCollidablePrimitiveObject;
        }

        public override void Reset()
        {
            base.Reset();

            projectileDirection = 1;
            this.BossLeewayCountdownInMs = 0;

            this.Transform.Rotation = Vector3.Zero;
        }
    }
}
