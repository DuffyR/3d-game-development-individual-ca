﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class ProjectileCollidablePrimitiveObject : CollidablePrimitiveObject
    {
        #region Fields
        private Vector3 moveDirection;
        private float moveSpeed;
        private ManagerParameters managerParameters;
        #endregion

        #region Properties
        public Vector3 MoveDirection
        {
            get
            {
                return this.moveDirection;
            }
            set
            {
                this.moveDirection = value;
                this.moveDirection.Normalize(); //Direction only
            }
        }

        public float MoveSpeed
        {
            get
            {
                return this.moveSpeed;
            }
            set
            {
                this.moveSpeed = Math.Abs(value); //Absolute value
            }
        }
        #endregion

        public ProjectileCollidablePrimitiveObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters,
            StatusType statusType, IVertexData vertexData, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters, Vector3 moveDirection, float moveSpeed) 
            : base(id, actorType, transform, effectParameters, statusType, vertexData, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveDirection = moveDirection;
            this.moveSpeed = moveSpeed;
            this.managerParameters = managerParameters;
        }

        public ProjectileCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive, 
            ManagerParameters managerParameters, Vector3 moveDirection, float moveSpeed)
            : base(primitiveObject, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveDirection = moveDirection;
            this.moveSpeed = moveSpeed;
            this.managerParameters = managerParameters;
        }

        public override void Update(GameTime gameTime)
        {
            this.Transform.TranslateIncrement = this.moveDirection * this.moveSpeed * gameTime.ElapsedGameTime.Milliseconds;

            //have we collided with something?
            this.Collidee = CheckCollisions(gameTime);

            //how do we respond to this collidee e.g. pickup?
            HandleCollisionResponse(this.Collidee);

            ApplyInput(gameTime);

            base.Update(gameTime);
        }

        protected override void HandleCollisionResponse(Actor collidee)
        {
            //Check if the projectile has collided with the boundaries of the game
            if (collidee is CollidablePrimitiveObject)
            {
                if (collidee.GroupParameters != null && collidee.GroupParameters.UniqueGroupID == AppData.ProjectileBoundaryGroupID)
                {
                    EventDispatcher.Publish(new EventData(this, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));
                }
            }
        }

        public override object GetDeepCopy()
        {
            ProjectileCollidablePrimitiveObject copy = new ProjectileCollidablePrimitiveObject(
                "Clone - " + ID,
                this.ActorType,
                (Transform3D)this.Transform.Clone(),
                (EffectParameters)this.EffectParameters.Clone(), //deep
                this.StatusType, //deep
                this.VertexData,
                (ICollisionPrimitive)this.CollisionPrimitive.Clone(),
                this.managerParameters,
                this.moveDirection,
                this.moveSpeed);

            if (this.ControllerList != null)
            {
                //clone each of the (behavioural) controllers
                foreach (IController controller in this.ControllerList)
                    copy.AttachController((IController)controller.Clone());
            }

            if (this.GroupParameters != null)
            {
                copy.GroupParameters = this.GroupParameters.Clone() as GroupParameters;
            }

            return copy;
        }
    }
}
