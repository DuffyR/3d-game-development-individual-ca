﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace GDLibrary
{
    public class PlayerCollidablePrimitiveObject : CollidablePrimitiveObject
    {
        #region Fields
        private float moveSpeed;
        private Keys[] moveKeys;

        private int deathSequenceTimerInMs = 0;
        private int speedPowerupTimerInMs = 0;
        private int sidewaysPowerupTimerInMs = 0;
        private bool bHasShield = false;

        private ProjectileCollidablePrimitiveObject playerProjectile;
        private ManagerParameters managerParameters;
        #endregion

        #region Properties
        public float MoveSpeed
        {
            get
            {
                return this.moveSpeed;
            }
            set
            {
                this.moveSpeed = value;
            }
        }
        #endregion

        public PlayerCollidablePrimitiveObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters,
            StatusType statusType, IVertexData vertexData, ICollisionPrimitive collisionPrimitive, 
            ManagerParameters managerParameters,
            Keys[] moveKeys, float moveSpeed, ProjectileCollidablePrimitiveObject projectile) 
            : base(id, actorType, transform, effectParameters, statusType, vertexData, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.playerProjectile = projectile;

            //for input
            this.managerParameters = managerParameters;
        }

        //used to make a player collidable primitives from an existing PrimitiveObject (i.e. the type returned by the PrimitiveFactory
        public PlayerCollidablePrimitiveObject(PrimitiveObject primitiveObject, ICollisionPrimitive collisionPrimitive,
            ManagerParameters managerParameters,
            Keys[] moveKeys, float moveSpeed, ProjectileCollidablePrimitiveObject projectile)
            : base(primitiveObject, collisionPrimitive, managerParameters.ObjectManager)
        {
            this.moveKeys = moveKeys;
            this.moveSpeed = moveSpeed;
            this.playerProjectile = projectile;

            //for input
            this.managerParameters = managerParameters;
        }


        public override void Update(GameTime gameTime)
        {
            //Effecitvely pause everything if the active camera is not the in-game camera
            if (this.managerParameters.CameraManager.ActiveCamera.ID.Equals(AppData.InGameCameraID))
            {
                if (deathSequenceTimerInMs <= 0)
                {
                    //read any input and store suggested increments
                    //But only if the correct camera is active
                    HandleInput(gameTime);

                    //have we collided with something?
                    this.Collidee = CheckCollisions(gameTime);

                    //how do we respond to this collidee e.g. pickup?
                    HandleCollisionResponse(this.Collidee);

                    //if no collision then move - see how we set this.Collidee to null in HandleCollisionResponse() 
                    //below when we hit against a zone
                    if (this.Collidee == null)
                        ApplyInput(gameTime);

                    //Decrease powerup-related timers
                    speedPowerupTimerInMs = (speedPowerupTimerInMs <= 0) ? 0 : speedPowerupTimerInMs - gameTime.ElapsedGameTime.Milliseconds;
                    sidewaysPowerupTimerInMs = (sidewaysPowerupTimerInMs <= 0) ? 0 : sidewaysPowerupTimerInMs - gameTime.ElapsedGameTime.Milliseconds;

                    //Change the diffuse color if the player has a shield
                    this.EffectParameters.DiffuseColor = (bHasShield) ? Color.DarkSlateBlue : Color.White;

                    //reset translate and rotate and update primitive
                    base.Update(gameTime);
                }
                else
                {
                    deathSequenceTimerInMs -= gameTime.ElapsedGameTime.Milliseconds;

                    if (deathSequenceTimerInMs <= 0)
                    {
                        object[] additionalParameters = { "Player Lives Controller", new Integer(-1) };
                        EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalParameters));
                        // Remove all projectiles belonging to a boss, so the player has enough time to move away
                        this.managerParameters.ObjectManager.Remove(actor => (actor.GroupParameters != null) && (actor.GroupParameters.UniqueGroupID == AppData.BossProjectileGroupID));
                        this.Transform.TranslateTo(new Vector3(0, 10, 0));
                    }
                }
            }
            
        }

        //this is where you write the application specific CDCR response for your game
        protected override void HandleCollisionResponse(Actor collidee)
        {
            if (collidee is CollidablePrimitiveObject)
            {
                //First, check if the collidee is a powerup
                //If not, check if the collidee is related to a boss
                if (collidee is ProjectileCollidablePrimitiveObject && collidee.GroupParameters.UniqueGroupID == AppData.PowerUpProjectileGroupID)
                {
                    //Respond to this powerup
                    ProjectileCollidablePrimitiveObject powerup = collidee as ProjectileCollidablePrimitiveObject;

                    if (powerup != null)
                    {
                        HandlePoweUpCollisionResponse(powerup);
                    }

                } else if (collidee is BossCollidablePrimitiveObject || (collidee is ProjectileCollidablePrimitiveObject && collidee.GroupParameters.UniqueGroupID == AppData.BossProjectileGroupID))
                {
                    HandleBossCollisionResponse(collidee);
                }
            }
        }

        private void HandleBossCollisionResponse(Actor collidee)
        {
            bool bCanKillPlayer = false;

            //If the collidee is a projectile, it should only kill if the player has no shield
            if (collidee is ProjectileCollidablePrimitiveObject)
            {
                //Send a event to remove this projectile
                EventDispatcher.Publish(new EventData(collidee, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));

                if (bHasShield)
                {
                    object[] additionalParameters = { "lose_shield" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));

                    bHasShield = false;
                } else
                {
                    bCanKillPlayer = true;
                }

            } else if (collidee is BossCollidablePrimitiveObject) //Colliding with a boss will kill you, shield or no shield
            {
                BossCollidablePrimitiveObject boss = collidee as BossCollidablePrimitiveObject;

                if (boss.BDefeated)
                {
                    this.Collidee = null;
                } else
                {
                    bCanKillPlayer = true;
                }
            }

            if (bCanKillPlayer)
            {
                deathSequenceTimerInMs = 1800;
                speedPowerupTimerInMs = 0;
                sidewaysPowerupTimerInMs = 0;
                this.Transform.TranslateTo(Vector3.Zero); //Move the player away from the screen

                object[] additionalParameters = { "player_death" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
        }

        private void HandlePoweUpCollisionResponse(ProjectileCollidablePrimitiveObject powerup)
        {
            //Find out which power up this is
            int subID = powerup.GroupParameters.UniqueSubGroupID;
            if (subID == AppData.PowerUpSpeedUpSubGroupID)
            {
                speedPowerupTimerInMs = 8000;
            } else if (subID == AppData.PowerUpSplitSubGroupID)
            {
                sidewaysPowerupTimerInMs = 10000;
            } else if (subID == AppData.PowerUpShieldSubGroupID)
            {
                bHasShield = true;
            }

            EventDispatcher.Publish(new EventData(powerup, EventActionType.OnRemoveActor, EventCategoryType.SystemRemove));

            object[] additionalParameters = { "get_powerup" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
        }

        protected override void HandleInput(GameTime gameTime)
        {
            // Increase the move speed if the respective powerup is active
            float speedMultiplier = (speedPowerupTimerInMs > 0) ? 1.25f : 1;

            //The player only moves - no rotation
            if (this.managerParameters.KeyboardManager.IsKeyDown(this.moveKeys[0])) //Up
            {
                this.Transform.TranslateIncrement = Vector3.UnitY * gameTime.ElapsedGameTime.Milliseconds * this.moveSpeed * speedMultiplier;
            } else if (this.managerParameters.KeyboardManager.IsKeyDown(this.moveKeys[1])) // Down
            {
                this.Transform.TranslateIncrement = -Vector3.UnitY * gameTime.ElapsedGameTime.Milliseconds * this.moveSpeed * speedMultiplier;
            }

            if (this.managerParameters.KeyboardManager.IsKeyDown(this.moveKeys[2])) // Left
            {
                this.Transform.TranslateIncrement += -Vector3.UnitX * gameTime.ElapsedGameTime.Milliseconds * this.moveSpeed * speedMultiplier;
            } else if (this.managerParameters.KeyboardManager.IsKeyDown(this.moveKeys[3])) // Right
            {
                this.Transform.TranslateIncrement += Vector3.UnitX * gameTime.ElapsedGameTime.Milliseconds * this.moveSpeed * speedMultiplier;
            }

            if (this.managerParameters.KeyboardManager.IsFirstKeyPress(Keys.Space))
            {
                ShootProjectile();
            }

            //The player rotates on their own
            this.Transform.RotateIncrement = (speedPowerupTimerInMs > 0) ? 5 : 1;
        }

        private void ShootProjectile()
        {
            //Find out how many projectiles we need to shoot
            List<Vector3[]> projectileParameters = GetProjectileParameters();

            foreach (var parameter in projectileParameters)
            {
                ProjectileCollidablePrimitiveObject shotProjectileObject = this.playerProjectile.Clone() as ProjectileCollidablePrimitiveObject;
                shotProjectileObject.Transform.Translation = this.Transform.Translation;
                shotProjectileObject.Transform.TranslateBy(parameter[1]);
                shotProjectileObject.Transform.ScaleTo(new Vector3(0.2f));
                shotProjectileObject.CollisionPrimitive = new SphereCollisionPrimitive(this.Transform, 0.4f);
                shotProjectileObject.ID = "Player Projectile";
                shotProjectileObject.EffectParameters.DiffuseColor = Color.Chocolate;
                shotProjectileObject.MoveDirection = parameter[0];
                shotProjectileObject.GroupParameters = new GroupParameters("PlayerProjectile", AppData.PlayerProjectileGroupID);

                EventDispatcher.Publish(new EventData(shotProjectileObject, EventActionType.OnAddActor, EventCategoryType.SystemAdd));
            }
            
            object[] additionalParameters = { "proj_shot" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
        }

        private List<Vector3[]> GetProjectileParameters()
        {
            List<Vector3[]> parameters = new List<Vector3[]>();

            Vector3[] regularProjectile = { Vector3.UnitY, new Vector3(0, 0.75f, 0) };
            parameters.Add(regularProjectile);

            if (sidewaysPowerupTimerInMs > 0)
            {
                Vector3[] leftSideProjectile = { -Vector3.UnitX, new Vector3(-0.75f, 0, 0) };
                parameters.Add(leftSideProjectile);

                Vector3[] rightSideProjectile = { Vector3.UnitX, new Vector3(0.75f, 0, 0) };
                parameters.Add(rightSideProjectile);
            }

            return parameters;
        }
    }
}
