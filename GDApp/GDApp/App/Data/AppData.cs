﻿/*
Function: 		Stores common hard-coded variable values used within the game e.g. key mappings, mouse sensitivity
Author: 		NMCG
Version:		1.0
Date Updated:	5/10/17
Bugs:			None
Fixes:			None
*/

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace GDLibrary
{
    public sealed class LerpSpeed
    {
        private static readonly float SpeedMultiplier = 2;
        public static readonly float VerySlow = 0.05f;
        public static readonly float Slow = SpeedMultiplier * VerySlow;
        public static readonly float Medium = SpeedMultiplier * Slow;
        public static readonly float Fast = SpeedMultiplier * Medium;
        public static readonly float VeryFast = SpeedMultiplier * Fast;
    }
    public sealed class AppData
    {
        #region Common
        public static int IndexMoveForward = 0;
        public static int IndexMoveBackward = 1;
        public static int IndexRotateLeft = 2;
        public static int IndexRotateRight = 3;
        public static int IndexMoveJump = 4;
        public static int IndexMoveCrouch = 5;
        public static int IndexStrafeLeft = 6;
        public static int IndexStrafeRight = 7;
        #endregion

        #region Camera
        public static readonly int CurveEvaluationPrecision = 4;

        public static readonly float CameraRotationSpeed = 0.0125f;
        public static readonly float CameraMoveSpeed = 0.025f;
        public static readonly float CameraStrafeSpeed = 0.6f * CameraMoveSpeed;
    
        //JigLib related collidable camera properties
        public static readonly float CollidableCameraJumpHeight = 12;
        public static readonly float CollidableCameraMoveSpeed = 0.6f;
        public static readonly float CollidableCameraStrafeSpeed = 0.6f * CollidableCameraMoveSpeed;
        public static readonly float CollidableCameraCapsuleRadius = 2;
        public static readonly float CollidableCameraViewHeight = 8; //how tall is the first person player?
        public static readonly float CollidableCameraMass = 10;

        public static readonly Keys[] CameraMoveKeys = { Keys.W, Keys.S, Keys.A, Keys.D, 
                                         Keys.Space, Keys.C, Keys.LeftShift, Keys.RightShift};
        public static readonly Keys[] CameraMoveKeys_Alt1 = { Keys.T, Keys.G, Keys.F, Keys.H };

        public static readonly float CameraThirdPersonScrollSpeedDistanceMultiplier = 0.00125f;
        public static readonly float CameraThirdPersonScrollSpeedElevationMultiplier = 0.01f;
        public static readonly float CameraThirdPersonDistance = 12;
        public static readonly float CameraThirdPersonElevationAngleInDegrees = 150;

        public static readonly float SecurityCameraRotationSpeedSlow = 0.5f;
        public static readonly float SecurityCameraRotationSpeedMedium = 2 * SecurityCameraRotationSpeedSlow;
        public static readonly float SecurityCameraRotationSpeedFast = 2 * SecurityCameraRotationSpeedMedium;

        //yaw means to rotate around the Y-axis - this will confuse you at first since we're using UnitX but you need to look at Transform3D::RotateBy()
        public static readonly Vector3 SecurityCameraRotationAxisYaw = Vector3.UnitX;
        public static readonly Vector3 SecurityCameraRotationAxisPitch = Vector3.UnitY;
        public static readonly Vector3 SecurityCameraRotationAxisRoll = Vector3.UnitZ;

        #endregion

        #region Player
        public static readonly string PlayerOneID = "player1";

        public static readonly Keys[] PlayerOneMoveKeys = { Keys.W, Keys.S, Keys.A, Keys.D, Keys.Y, Keys.I, Keys.N, Keys.M};
        public static readonly float PlayerMoveSpeed = 0.02f;
        public static readonly float PlayerStrafeSpeed = 0.7f * PlayerMoveSpeed;
        public static readonly float PlayerRotationSpeed = 0.08f;

        #endregion

        #region Menu
        public static readonly Keys KeyPauseShowMenu = Keys.Escape;
        public static readonly Keys KeyToggleCameraLayout = Keys.F1;
        #endregion

        #region Mouse
        //defines how much the mouse has to move in pixels before a movement is registered - see MouseManager::HasMoved()
        public static readonly float MouseSensitivity = 1;

        //always ensure that we start picking OUTSIDE the collidable first person camera radius - otherwise we will always pick ourself!
        public static readonly float PickStartDistance = CollidableCameraCapsuleRadius * 2f;
        public static readonly float PickEndDistance = 1000; //can be related to camera far clip plane radius but should be limited to typical level max diameter
        public static readonly bool EnablePickAndPlace = true;

        #endregion

        #region UI
        public static readonly string PlayerOneProgressID = PlayerOneID + " progress";
        public static readonly string PlayerOneProgressControllerID = PlayerOneProgressID + " ctrllr";
        #endregion

        #region JigLibX
        public static readonly Vector3 Gravity = -10 * Vector3.UnitY;
        public static readonly Vector3 BigGravity = 5 * Gravity;

        #endregion


        #region Video
        public static readonly string VideoIDMainHall;
        public static readonly string ControllerIDSuffix = " controller";
        #endregion

        #region Primitive IDs
        public static readonly string TexturedQuadID = "textured quad";
        public static readonly string TexturedBillboardQuadID = "textured billboard quad";
        public static readonly string OriginHelperID = "XYZ origin helper";
        public static readonly string TexturedCubeID = "textured cube";
        #endregion

        #region Effect IDs
        //used by primitives based on VertexPositionColor vertex type
        public static readonly string UnLitColoredPrimitivesEffectID = "unlit colored";

        //used by primitives based on VertexPositionColorTexture vertex type
        public static readonly string UnLitTexturedPrimitivesEffectID = "unlit textured";

        //used by primitives based on VertexBillboard vertex type
        public static readonly string UnlitBillboardsEffectID = "unlit textured billboards";

        //used by primitives based on VertexPositionNormalTexture vertex type
        public static readonly string LitTexturedPrimitivesEffectID = "lit textured ";
        #endregion

        #region Camera IDs
        public static readonly string FlightCameraID = "FlightCameraID";
        public static readonly string InGameCameraID = "In-Game Camera";
        public static readonly string IntroCameraID = "Intro Camera";
        #endregion

        #region Zone IDs

        #endregion

        #region Boundary Data
        public static readonly Vector3[] boundaryPositions = {
            //Player Boundaries
            new Vector3(14.5f, 16, 0),
            new Vector3(-14.5f, 16, 0),
            new Vector3(0, 24.1f, 0),
            new Vector3(0, 7.75f, 0),

            //Projectile Boundaries
            new Vector3(0, 33, 0),
            new Vector3(0, -5, 0),
            new Vector3(20, 14, 0),
            new Vector3(-20, 14, 0)
        };

        public static readonly Vector3[] boundaryScales =
        {
            //Player Boundaries
            new Vector3(1, 15, 1),
            new Vector3(1, 15, 1),
            new Vector3(28, 1, 1),
            new Vector3(28, 1, 1),

            //Projectile Boundaries
            new Vector3(40, 1, 1),
            new Vector3(40, 1, 1),
            new Vector3(1, 37, 1),
            new Vector3(1, 37, 1)
        };
        #endregion

        #region Group IDs
        public static readonly int PlayerBoundaryGroupID = 1;
        public static readonly int ProjectileBoundaryGroupID = 2;
        public static readonly int PlayerProjectileGroupID = 3;
        public static readonly int BossProjectileGroupID = 4;

        #region Projectile Groups & Sub-Goups
        public static readonly int PowerUpProjectileGroupID = 5;

        public static readonly int PowerUpSpeedUpSubGroupID = 1;
        public static readonly int PowerUpSplitSubGroupID = 2;
        public static readonly int PowerUpShieldSubGroupID = 3;
        #endregion
        #endregion
    }
}
