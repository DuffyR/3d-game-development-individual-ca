#define DEMO

using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;
using System;

/*
 * Diffuse color not pickup up when drawing primitive objects
 * TranslationLerpController
 * Is frustum culling working on primitive objects?
 * 
 * 
 * No statustype on controllers
 * mouse object text interleaving with progress controller
 * Z-fighting on ground plane in 3rd person mode
 * Elevation angle on 3rd person view
 * PiP
 * menu transparency
*/

namespace GDApp
{
    public class Main : Game
    {
        #region Statics
        private readonly Color GoogleGreenColor = new Color(152, 234, 224, 225);
        #endregion

        #region Fields
#if DEBUG
        //used to visualize debug info (e.g. FPS) and also to draw collision skins
        private DebugDrawer debugDrawer;
#endif

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //added property setters in short-hand form for speed
        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public UIManager uiManager { get; private set; }
        public GamePadManager gamePadManager { get; private set; }
        public SoundManager soundManager { get; private set; }
        public MyGameStateManager stateManager { get; private set; }

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        //stores loaded game resources
        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;

        //stores curves and rails used by cameras, viewport, effect parameters
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        private Dictionary<string, Viewport> viewPortDictionary;
        private Dictionary<string, EffectParameters> effectDictionary;
        private ContentDictionary<Video> videoDictionary;
        private Dictionary<string, IVertexData> vertexDataDictionary;

        private ManagerParameters managerParameters;
        private MyPrimitiveFactory primitiveFactory;
        private PlayerCollidablePrimitiveObject playerCollidablePrimitiveObject;
        private PrimitiveDebugDrawer collisionSkinDebugDrawer;



        #endregion

        #region Properties
        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            //moved instanciation here to allow menu and ui managers to be moved to InitializeManagers()
            spriteBatch = new SpriteBatch(GraphicsDevice);

            bool isMouseVisible = true;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;
            int numberOfGamePadPlayers = 1;

            //set the title
            Window.Title = "Rainbow Attack";

            //EventDispatcher
            InitializeEventDispatcher();

            //Dictionaries, Media Assets and Non-media Assets
            LoadDictionaries();
            LoadAssets();
            LoadCurvesAndRails();
            LoadViewports(screenResolution);

            //factory to produce primitives
            LoadFactories();

            //Effects
            InitializeEffects();

            //Managers
            InitializeManagers(screenResolution, screenType, isMouseVisible, numberOfGamePadPlayers);

            //Add Menu and UI elements
            AddMenuElements();
            AddUIElements();

            //Load related elements to our game
            LoadGame();

            LoadGameCameras(screenResolution);

            //Publish Start Event(s)
            StartGame();

#if DEBUG
            //InitializeDebugTextInfo();
            InitializeDebugCollisionSkinInfo();
#endif

            base.Initialize();
        }

       
        private void InitializeManagers(Integer2 screenResolution, 
            ScreenUtility.ScreenType screenType, bool isMouseVisible, int numberOfGamePadPlayers) //1 - 4
        {
            //add sound manager
            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/", "Demo2DSound.xgs", "WaveBank1.xwb", "SoundBank1.xsb");
            Components.Add(this.soundManager);

            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);

            //create the object manager - notice that its not a drawablegamecomponent. See ScreeManager::Draw()
            this.objectManager = new ObjectManager(this, this.cameraManager, this.eventDispatcher, 10);
            
            //add keyboard manager
            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution, screenType,
                this.objectManager, this.cameraManager, this.keyboardManager,
                AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            this.screenManager.DrawOrder = 0;
            Components.Add(this.screenManager);
      
            //add mouse manager
            this.mouseManager = new MouseManager(this, isMouseVisible);
            Components.Add(this.mouseManager);

            //add gamepad manager
            if (numberOfGamePadPlayers > 0)
            {
                this.gamePadManager = new GamePadManager(this, numberOfGamePadPlayers);
                Components.Add(this.gamePadManager);
            }

            //menu manager
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, spriteBatch, this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("mainmenu");
            this.menuManager.DrawOrder = 3;
            Components.Add(this.menuManager);

            //ui (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            this.uiManager.DrawOrder = 4;
            Components.Add(this.uiManager);
        
            //this object packages together all managers to give the mouse object the ability to listen for all forms of input from the user, as well as know where camera is etc.
            this.managerParameters = new ManagerParameters(this.objectManager,
                this.cameraManager, this.mouseManager, this.keyboardManager, this.gamePadManager, this.screenManager, this.soundManager);

            this.stateManager = new MyGameStateManager(this, this.eventDispatcher, StatusType.Off, this.menuManager, this.managerParameters);
            Components.Add(this.stateManager);

        }

        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();

            //stores default effect parameters
            this.effectDictionary = new Dictionary<string, EffectParameters>();

            //notice we go back to using a content dictionary type since we want to pass strings and have dictionary load content
            this.videoDictionary = new ContentDictionary<Video>("video dictionary", this.Content);

            //used to store IVertexData (i.e. when we want to draw primitive objects, as in I-CA)
            this.vertexDataDictionary = new Dictionary<string, IVertexData>();

        }
        private void LoadAssets()
        {
            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate1"); //demo use of the shorter form of Load() that generates key from asset name
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate2");
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/grass1");
            this.textureDictionary.Load("Assets/Textures/Skybox/back");
            this.textureDictionary.Load("Assets/Textures/Skybox/left");
            this.textureDictionary.Load("Assets/Textures/Skybox/right");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky");
            this.textureDictionary.Load("Assets/Textures/Skybox/front");
            this.textureDictionary.Load("Assets/Textures/Foliage/Trees/tree2");

            this.textureDictionary.Load("Assets/Textures/Architecture/sky_and_sea");

            this.textureDictionary.Load("Assets/Textures/Semitransparent/transparentstripes");

            this.textureDictionary.Load("Assets/Textures/Props/player");
            this.textureDictionary.Load("Assets/Textures/Props/boss");
            this.textureDictionary.Load("Assets/Textures/Props/powerup_speed");
            this.textureDictionary.Load("Assets/Textures/Props/powerup_split");
            this.textureDictionary.Load("Assets/Textures/Props/powerup_shield");


            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/genericbtn");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/menubutton_RD");

            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/mainmenu_RD");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu_RD");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu_RD");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/nextlevelmenu_RD");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/winmenu_RD");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/winmenu_RD_easteregg");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/losemenu_RD");

            //ui (or hud) elements
            this.textureDictionary.Load("Assets/Textures/UI/HUD/reticuleDefault");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/progress_white");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/progress_gradient_RD");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/player_health");


            //dual texture demo - see Main::InitializeCollidableGround()
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard_greywhite");

            //levels
            this.textureDictionary.Load("Assets/Textures/Level/level1");


#if DEBUG
            //demo
            this.textureDictionary.Load("Assets/GDDebug/Textures/ml");
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard");
#endif
            #endregion

            #region Fonts
#if DEBUG
            this.fontDictionary.Load("Assets/GDDebug/Fonts/debug");
#endif
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            #endregion

            #region Video
            this.videoDictionary.Load("Assets/Video/sample");
            #endregion
        }
        private void LoadCurvesAndRails()
        {
            #region Curves
            //create the camera curve to be applied to the track controller
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Oscillate); //experiment with other CurveLoopTypes
            curveA.Add(new Vector3(0.1f, 12.4f, 5.4f), new Vector3(0, -0.4f, -0.9f), new Vector3(0, 0.9f, -0.4f), 0); //start position
            curveA.Add(new Vector3(0.1f, 12.4f, 5.4f), new Vector3(0, -0.4f, -0.9f), new Vector3(0, 0.9f, -0.4f), 2);
            curveA.Add(new Vector3(0, 17.4f, 7.4f), new Vector3(0, 0.5f, -0.9f), new Vector3(0, 0.9f, 0.5f), 3);
            curveA.Add(new Vector3(0, 17.4f, 7.4f), new Vector3(0, 0.5f, -0.9f), new Vector3(0, 0.9f, 0.5f), 5);
            curveA.Add(new Vector3(0, 17, 20), -Vector3.UnitZ, Vector3.UnitY, 7);
            //add to the dictionary
            this.curveDictionary.Add("Game Intro Curve", curveA);
            #endregion

        }
        private void LoadViewports(Integer2 screenResolution)
        {

            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);

            //work out the dimensions of the small camera views along the left hand side of the screen
            int smallViewPortHeight = 144; //6 small cameras along the left hand side of the main camera view i.e. total height / 5 = 720 / 5 = 144
            int smallViewPortWidth = 5 * smallViewPortHeight / 3; //we should try to maintain same ProjectionParameters aspect ratio for small cameras as the large     
            //the five side viewports in multi-screen mode
            this.viewPortDictionary.Add("column0 row0", new Viewport(0, 0, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row1", new Viewport(0, 1 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row2", new Viewport(0, 2 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row3", new Viewport(0, 3 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row4", new Viewport(0, 4 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            //the larger view to the right in column 1
            this.viewPortDictionary.Add("column1 row0", new Viewport(smallViewPortWidth, 0, screenResolution.X - smallViewPortWidth, screenResolution.Y));

            //picture-in-picture viewport
            Integer2 viewPortDimensions = new Integer2(240, 150); //set to 16:10 ratio as with screen dimensions
            int verticalOffset = 20;
            int rightHorizontalOffset = 20;
            this.viewPortDictionary.Add("PIP viewport", new Viewport((screenResolution.X - viewPortDimensions.X - rightHorizontalOffset), 
                verticalOffset,  viewPortDimensions.X, viewPortDimensions.Y));
        }
        private void LoadFactories()
        {
            this.primitiveFactory = new MyPrimitiveFactory();
        }

#if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.managerParameters, spriteBatch,
                this.fontDictionary["debug"], Color.Black, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            this.debugDrawer.DrawOrder = 1;
            Components.Add(this.debugDrawer);

        }

        //draws the frustum culling spheres, the collision primitive surfaces, and the zone object collision primitive surfaces based on booleans passed
        private void InitializeDebugCollisionSkinInfo()
        {
            int primitiveCount = 0;
            PrimitiveType primitiveType;

            //used to draw spherical collision surfaces
            IVertexData sphereVertexData = new BufferedVertexData<VertexPositionColor>(
                graphics.GraphicsDevice, PrimitiveUtility.GetWireframeSphere(5, out primitiveType, out primitiveCount), primitiveType, primitiveCount);

            this.collisionSkinDebugDrawer = new PrimitiveDebugDrawer(this, this.eventDispatcher, StatusType.Update | StatusType.Drawn,
                this.managerParameters, false, false, false, sphereVertexData);
            collisionSkinDebugDrawer.DrawOrder = 2;
            Components.Add(collisionSkinDebugDrawer);

        }
#endif
        #endregion

        private void LoadGame()
        {
            //Load a helper to orient ourselves
            //InitializeHelperPrimitives();

            //Create a base projectile for any primitiveobjects to use
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitColoredPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.WireframeSphere, effectParameters);
            Transform3D transform = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(1), -Vector3.UnitZ, Vector3.UnitY);
            ProjectileCollidablePrimitiveObject projectilePrimitive = new ProjectileCollidablePrimitiveObject(
                primitiveObject, new SphereCollisionPrimitive(transform, 1),
                this.managerParameters, Vector3.UnitY, 0.02f);

            InitializePrimitiveBoundaries();

            InitializeBackgroudPrimitive();

            InitializePlayerPrimitive(projectilePrimitive);

            InitializePowerUpPrimitives(projectilePrimitive);

            InitializeRedBossPrimitives(projectilePrimitive, "Red Boss", Color.Red,
                new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, Vector3.One * 3, -Vector3.UnitZ, Vector3.UnitY),
                45, 3, 1312, 1504);
            InitializeOrangeBossPrimitives(projectilePrimitive, "Orange Boss", Color.Orange, 50, 5, 35);
            InitializeYellowBossPrimitives(projectilePrimitive, "Yellow Boss", Color.Yellow,
                new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, new Vector3(2, 2.5f, 2), -Vector3.UnitZ, Vector3.UnitY),
                50, 2);
            InitializeRedBossPrimitives(projectilePrimitive, "Green Boss", Color.Green,
                new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, new Vector3(3, 1.5f, 1.5f), -Vector3.UnitZ, Vector3.UnitY),
                64, 2, 800, 1200);
            InitializeYellowBossPrimitives(projectilePrimitive, "Blue Boss", Color.Blue,
                new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, Vector3.One * 2, -Vector3.UnitZ, Vector3.UnitY),
                50, 2);
            InitializeOrangeBossPrimitives(projectilePrimitive, "Indigo Boss", Color.Indigo, 84, 3, 15);
            InitializeVioletBossPrimitive(projectilePrimitive);
        }

        private void InitializePrimitiveBoundaries()
        {
            //Get the relevant effect
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitColoredPrimitivesEffectID] as BasicEffectParameters;

            //Get the base object
            PrimitiveObject primitiveBase = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.ColoredCube, effectParameters);
            primitiveBase.EffectParameters.DiffuseColor = Color.GreenYellow;
            primitiveBase.ActorType = ActorType.CollidableDecorator;
            primitiveBase.StatusType = StatusType.Update;

            //Plce the boundaries via cloning
            for (int i = 0; i < AppData.boundaryPositions.Length; i++)
            {
                Transform3D transform = new Transform3D(AppData.boundaryPositions[i], AppData.boundaryScales[i]);
                CollidablePrimitiveObject collidableBoundary = new CollidablePrimitiveObject(primitiveBase.Clone() as PrimitiveObject,
                new BoxCollisionPrimitive(transform), this.objectManager);
                collidableBoundary.Transform = transform;

                if (i < 4) //Is a player boundary
                {
                    collidableBoundary.ID = "Player Boundary Primitive " + i;
                    collidableBoundary.GroupParameters = new GroupParameters("PlayerBoundary", AppData.PlayerBoundaryGroupID);
                } else //Must be a projectile boundary
                {
                    collidableBoundary.ID = "Projectile Boundary Primitive " + (i - 4);
                    collidableBoundary.GroupParameters = new GroupParameters("ProjectileBoundary", AppData.ProjectileBoundaryGroupID);
                }

                this.objectManager.Add(collidableBoundary);
            }

            
        }

        private void InitializeBackgroudPrimitive()
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitTexturedPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.TexturedQuad, effectParameters);
            primitiveObject.Transform.TranslateTo(new Vector3(0, 16, -40));
            primitiveObject.Transform.ScaleTo(new Vector3(150, 150, 1));
            primitiveObject.EffectParameters.Texture = this.textureDictionary["sky_and_sea"];

            this.objectManager.Add(primitiveObject);
        }

        private void InitializePlayerPrimitive(ProjectileCollidablePrimitiveObject projectile)
        {
            //Load a collidable model for our player
            // Get the correct effects, transform, collision primitive
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalDiamond, effectParameters);
            Transform3D transform = new Transform3D(new Vector3(0, 10, 0), Vector3.Zero, Vector3.One, -Vector3.UnitZ, Vector3.UnitY);//Transform3D.Zero;
            BoxCollisionPrimitive collisionPrimitive = new BoxCollisionPrimitive(transform);

            // Use variables to make the player
            this.playerCollidablePrimitiveObject = new PlayerCollidablePrimitiveObject(
                primitiveObject, collisionPrimitive,
                this.managerParameters, AppData.PlayerOneMoveKeys,
                AppData.PlayerMoveSpeed, projectile);
            this.playerCollidablePrimitiveObject.ActorType = ActorType.Player;
            this.playerCollidablePrimitiveObject.Transform = transform;
            playerCollidablePrimitiveObject.EffectParameters.Texture = this.textureDictionary["player"];
            playerCollidablePrimitiveObject.ID = "Main Player";

            this.objectManager.Add(playerCollidablePrimitiveObject);
        }

        private void InitializePowerUpPrimitives(ProjectileCollidablePrimitiveObject projectile)
        {
            //Create the 'model' that will spawn the powerups
            BasicEffectParameters spawnerEffectParameters = this.effectDictionary[AppData.UnLitColoredPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject powerUpSpawner = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.WireframeLine, spawnerEffectParameters);
            powerUpSpawner.StatusType = StatusType.Update; //Not drawn

            //Create the controller that handles the spawning of the powerups
            PowerUpSpawnController spawnController = new PowerUpSpawnController("Powerup Spawn Controller", ControllerType.Spawner, 2500, 7, 3);

            //Give the controller all the nessecary powerups, identified by their subgroup IDs
            for (int i = 0; i < 3; i++)
            {
                //Properties that all powerups share
                BasicEffectParameters powerupEffectParameters = this.effectDictionary[AppData.UnLitTexturedPrimitivesEffectID] as BasicEffectParameters;
                PrimitiveObject powerupModel = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.TexturedQuad, powerupEffectParameters);
                Transform3D transform = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(2, 2, 1), -Vector3.UnitZ, Vector3.UnitY);
                ProjectileCollidablePrimitiveObject powerup = new ProjectileCollidablePrimitiveObject(powerupModel, new SphereCollisionPrimitive(transform, 1),
                    this.managerParameters, Vector3.UnitX, 0.0075f);
                powerup.Transform = transform;
                powerup.GroupParameters = new GroupParameters("Powerup", AppData.PowerUpProjectileGroupID);

                //powerup specific properties
                switch (i)
                {
                    case 0:
                        powerup.ID = "Speed-up Powerup";
                        //powerup.EffectParameters.DiffuseColor = Color.Red;
                        powerup.EffectParameters.Texture = this.textureDictionary["powerup_speed"];
                        powerup.GroupParameters.UniqueSubGroupID = AppData.PowerUpSpeedUpSubGroupID;
                        break;
                    case 1:
                        powerup.ID = "Split Powerup";
                        //powerup.EffectParameters.DiffuseColor = Color.Green;
                        powerup.EffectParameters.Texture = this.textureDictionary["powerup_split"];
                        powerup.GroupParameters.UniqueSubGroupID = AppData.PowerUpSplitSubGroupID;
                        break;
                    case 2:
                        powerup.ID = "Shield Powerup";
                        //powerup.EffectParameters.DiffuseColor = Color.Blue;
                        powerup.EffectParameters.Texture = this.textureDictionary["powerup_shield"];
                        powerup.GroupParameters.UniqueSubGroupID = AppData.PowerUpShieldSubGroupID;
                        break;
                    default:
                        powerup.ID = "Speed-up Powerup";
                        //powerup.EffectParameters.DiffuseColor = Color.Red;
                        powerup.EffectParameters.Texture = this.textureDictionary["powerup_speed"];
                        powerup.GroupParameters.UniqueSubGroupID = AppData.PowerUpSpeedUpSubGroupID;
                        break;
                }

                spawnController.AddProjectile(powerup);
            }

            //Attach the controller to the spawner
            powerUpSpawner.AttachController(spawnController);

            this.objectManager.Add(powerUpSpawner);
        }

        private void InitializeRedBossPrimitives(ProjectileCollidablePrimitiveObject projectile, string bossID, Color color, Transform3D transform,
            int health, int phases, int regularAttackInterval, int phaseThreeAttackInterval)
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);

            RedBossCollidablePrimitiveObject bossPrimitiveObject = new RedBossCollidablePrimitiveObject(primitiveObject, new BoxCollisionPrimitive(transform), this.managerParameters, health, phases, regularAttackInterval, phaseThreeAttackInterval);
            bossPrimitiveObject.ID = bossID;
            bossPrimitiveObject.Transform = transform;
            bossPrimitiveObject.EffectParameters.DiffuseColor = color;
            bossPrimitiveObject.EffectParameters.Texture = this.textureDictionary["boss"];

            bossPrimitiveObject.AddNewProjectile("Regular Attack", projectile);

            //this.objectManager.Add(bossPrimitiveObject);
            this.stateManager.AddBoss(bossPrimitiveObject);
        }

        private void InitializeOrangeBossPrimitives(ProjectileCollidablePrimitiveObject projectile, string bossID, Color color,
            int health, int phases, int phaseTwoShotAngle)
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            Transform3D transform = new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, Vector3.One * 2.75f, -Vector3.UnitZ, Vector3.UnitY);
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);

            OrangeBossCollidablePrimitiveObject bossPrimitiveObject = new OrangeBossCollidablePrimitiveObject(primitiveObject, new BoxCollisionPrimitive(transform), this.managerParameters, health, phases, phaseTwoShotAngle);
            bossPrimitiveObject.ID = bossID;
            bossPrimitiveObject.Transform = transform;
            bossPrimitiveObject.EffectParameters.DiffuseColor = color;
            bossPrimitiveObject.EffectParameters.Texture = this.textureDictionary["boss"];

            bossPrimitiveObject.AddNewProjectile("Regular Attack", projectile);

            //this.objectManager.Add(bossPrimitiveObject);
            this.stateManager.AddBoss(bossPrimitiveObject);
        }

        private void InitializeYellowBossPrimitives(ProjectileCollidablePrimitiveObject projectile, string bossID, Color color, Transform3D transform,
            int health, int phases)
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);

            YellowBossCollidablePrimitiveObject bossPrimitiveObject = new YellowBossCollidablePrimitiveObject(primitiveObject, new BoxCollisionPrimitive(transform), this.managerParameters, health, phases, AppData.PlayerOneMoveKeys);
            bossPrimitiveObject.ID = bossID;
            bossPrimitiveObject.Transform = transform;
            bossPrimitiveObject.EffectParameters.DiffuseColor = color;
            bossPrimitiveObject.EffectParameters.Texture = this.textureDictionary["boss"];

            bossPrimitiveObject.AddNewProjectile("Regular Attack", projectile);

            //A special projectile for this boss
            float[] distanceToCover1 = { 13 };

            bossPrimitiveObject.AddNewProjectile("Tracing Attack",
                InitializeYellowProjectile("Yellow Boss Tracing Projectile", projectile,
                new GroupParameters("BossProjectile", AppData.BossProjectileGroupID), Color.YellowGreen, 1, distanceToCover1, 0.03f, 800));

            float[] distanceToCover2 = { 13, 12 };

            bossPrimitiveObject.AddNewProjectile("Tracing Attack Type 2",
                InitializeYellowProjectile("Yellow Boss Tracing Type 2 Projectile", projectile,
                new GroupParameters("BossProjectile", AppData.BossProjectileGroupID), Color.Yellow, 2, distanceToCover2, 0.03f, 800));
            

            //A controller to give the boss a slight bobbing effect
            bossPrimitiveObject.AttachController(new TranslationSineLerpController("Yellow Boss Translation Lerp", ControllerType.LerpTranslation,
                new Vector3(0, 1, 0), new TrigonometricParameters(0.75f, 0.2f, 0)));

            //this.objectManager.Add(bossPrimitiveObject);
            this.stateManager.AddBoss(bossPrimitiveObject);
        }

        private void InitializeVioletBossPrimitive(ProjectileCollidablePrimitiveObject projectile)
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitTexturedPrimitivesEffectID] as BasicEffectParameters;
            PrimitiveObject primitiveObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.NormalCube, effectParameters);
            Transform3D transform = new Transform3D(new Vector3(0, 21, 0), Vector3.Zero, Vector3.One * 4, -Vector3.UnitZ, Vector3.UnitY);

            YellowBossCollidablePrimitiveObject bossPrimitiveObject = new YellowBossCollidablePrimitiveObject(primitiveObject, new BoxCollisionPrimitive(transform), this.managerParameters, 99, 3, AppData.PlayerOneMoveKeys);
            bossPrimitiveObject.ID = "Violet Boss";
            bossPrimitiveObject.Transform = transform;
            bossPrimitiveObject.EffectParameters.DiffuseColor = Color.Violet;
            bossPrimitiveObject.EffectParameters.Texture = this.textureDictionary["boss"];

            bossPrimitiveObject.AddNewProjectile("Regular Attack", projectile);

            //A special projectile for this boss
            float[] distanceToCover1 = { 13, 12 };

            bossPrimitiveObject.AddNewProjectile("Tracing Attack",
                InitializeYellowProjectile("Violet Boss Tracing Projectile", projectile,
                new GroupParameters("BossProjectile", AppData.BossProjectileGroupID), Color.Violet, 2, distanceToCover1, 0.05f, 450));

            float[] distanceToCover2 = { 13, 12, 28 };

            bossPrimitiveObject.AddNewProjectile("Tracing Attack Type 2",
                InitializeYellowProjectile("Violet Boss Tracing Type 2 Projectile", projectile,
                new GroupParameters("BossProjectile", AppData.BossProjectileGroupID), Color.DarkViolet, 3, distanceToCover2, 0.05f, 350));


            //A controller to give the boss a slight bobbing effect
            bossPrimitiveObject.AttachController(new TranslationSineLerpController("Violet Boss Translation Lerp", ControllerType.LerpTranslation,
                new Vector3(0, 1, 0), new TrigonometricParameters(0.5f, 0.3f, 0)));

            //this.objectManager.Add(bossPrimitiveObject);
            this.stateManager.AddBoss(bossPrimitiveObject);
        }

        private ProjectileCollidablePrimitiveObject InitializeYellowProjectile(string id, ProjectileCollidablePrimitiveObject projectile, GroupParameters groupParameters, Color color,
            int phases, float[] distanceToCover, float travelSpeed, int waitTime)
        {
            ProjectileCollidablePrimitiveObject tracingProjectile = projectile.Clone() as ProjectileCollidablePrimitiveObject;
            tracingProjectile.ID = id;
            tracingProjectile.GroupParameters = groupParameters;
            tracingProjectile.EffectParameters.DiffuseColor = color;
            tracingProjectile.AttachController(new YellowBossProjectileController("Yellow Tracing Controller", ControllerType.Projectile,
                phases, travelSpeed, distanceToCover, waitTime, YellowBossProjectileRotationDirection.Clockwise));

            return tracingProjectile;
        }

        #region Layout Helpers
        private void InitializeHelperPrimitives()
        {
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitColoredPrimitivesEffectID] as BasicEffectParameters;
            //since its wireframe we dont set color, texture, alpha etc.

            //get the archetype from the factory
            PrimitiveObject archetypeObject = this.primitiveFactory.GetArchetypePrimitiveObject(graphics.GraphicsDevice, ShapeType.WireframeOrigin, effectParameters);

            //clone to set the unique properties of the origin helper
            PrimitiveObject originObject = archetypeObject.Clone() as PrimitiveObject;

            //make it a little more visible!
            originObject.Transform.Translation = new Vector3(0, 0, 0);
            originObject.Transform.Scale *= 4;

            //set an ID if we want to access this later
            originObject.ID = "origin helper";

            //add to the object manager
            this.objectManager.Add(originObject);

        }
        #endregion

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, IController controller, float drawDepth)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardShallowSixteenNine, viewPort, drawDepth, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        private void LoadGameCameras(Integer2 screenResolution)
        {
            #region Flight Camera
            Transform3D transform = new Transform3D(new Vector3(0, 17, 20), -Vector3.UnitZ, Vector3.UnitY);

            IController controller = new FlightCameraController("fcc", ControllerType.FirstPerson, AppData.CameraMoveKeys,
                AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed, this.managerParameters);

            InitializeCamera(screenResolution, AppData.FlightCameraID, this.viewPortDictionary["full viewport"], transform, controller, 0);
            #endregion

            #region In-game static camera
            transform = new Transform3D(new Vector3(0, 17, 20), -Vector3.UnitZ, Vector3.UnitY);
            InitializeCamera(screenResolution, AppData.InGameCameraID, this.viewPortDictionary["full viewport"], transform, null, 0);
            #endregion

            #region Intro Curve Camera
            transform = new Transform3D(new Vector3(0, 17, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController("Intro Curve Controller", ControllerType.Track, this.curveDictionary["Game Intro Curve"], PlayStatusType.Off);
            InitializeCamera(screenResolution, AppData.IntroCameraID, this.viewPortDictionary["full viewport"], transform, controller, 0);
            #endregion
        }
        #endregion

        #region Events
        private void InitializeEventDispatcher()
        {
            //initialize with an arbitrary size based on the expected number of events per update cycle, increase/reduce where appropriate
            this.eventDispatcher = new EventDispatcher(this, 20);

            //dont forget to add to the Component list otherwise EventDispatcher::Update won't get called and no event processing will occur!
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { AppData.IntroCameraID };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
            //we could also just use the line below, but why not use our event dispatcher?
            //this.cameraManager.SetActiveCamera(x => x.ID.Equals("collidable first person camera 1"));
        }
        #endregion

        #region Menu & UI
        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 70;

            #region Main Menu
            sceneID = "main menu";

            //retrieve the background texture
            texture = this.textureDictionary["mainmenu_RD"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "startbtn";
            buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 4.0f, 200);
            texture = this.textureDictionary["menubutton_RD"];
            transform = new Transform2D(position,
                0, new Vector2(1.8f, 0.6f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.DodgerBlue, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.2f, 0.4f, 270)));
            uiButtonObject.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
                    new TrigonometricParameters(1, 0.4f, 270), uiButtonObject.Color, Color.White));
            this.menuManager.Add(sceneID, uiButtonObject);


            //add audio button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "audiobtn";
            clone.Text = "Audio";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            this.menuManager.Add(sceneID, clone);

            //add controls button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "controlsbtn";
            clone.Text = "Controls";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "exitbtn";
            clone.Text = "Exit";
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            //Change its colors
            clone.Color = Color.PaleVioletRed;
            clone.OriginalColor = clone.Color;
            UIColorSineLerpController colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu_RD"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add volume up button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            clone.ID = "volumeUpbtn";
            clone.Text = "Volume Up";
            this.menuManager.Add(sceneID, clone);

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "volumeUnMutebtn";
            clone.Text = "Volume Un-mute";
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 4 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //Change its colors
            clone.Color = Color.PaleVioletRed;
            clone.OriginalColor = clone.Color;
            colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["controlsmenu_RD"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(graphics.PreferredBackBufferWidth / 4.0f, 7 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //Change its colors
            clone.Color = Color.PaleVioletRed;
            clone.OriginalColor = clone.Color;
            colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Lose Menu
            sceneID = "lose menu";

            //retrieve the background texture
            texture = this.textureDictionary["losemenu_RD"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("losemenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //Add return button
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.ID = "backbtn";
            clone.Text = "Return";
            this.menuManager.Add(sceneID, clone);

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.ID = "exitbtn";
            clone.Text = "Give Up";
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            //Change its colors
            clone.Color = Color.IndianRed;
            clone.OriginalColor = clone.Color;
            colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Win Menu
            sceneID = "win menu";

            //retrieve the background texture
            Random r = new Random();
            texture = (r.Next(100) > 5) ? this.textureDictionary["winmenu_RD"] : this.textureDictionary["winmenu_RD_easteregg"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("winmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.ID = "exitbtn";
            clone.Text = "You're winner";
            //Change its colors
            clone.Color = Color.DarkGreen;
            clone.OriginalColor = clone.Color;
            colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Next Level Menu
            sceneID = "next level menu";

            //retrieve the background texture
            texture = this.textureDictionary["nextlevelmenu_RD"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);

            this.menuManager.Add(sceneID, new UITextureObject("nextlevelmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add exit button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.ID = "startbtn";
            clone.Text = "Next Level";
            //Change its colors
            clone.Color = Color.LightGreen;
            clone.OriginalColor = clone.Color;
            colorLerp = clone.ControllerList.Find(c => c is UIColorSineLerpController) as UIColorSineLerpController;
            colorLerp.ColorMin = clone.Color;
            this.menuManager.Add(sceneID, clone);
            #endregion
        }
        private void AddUIElements()
        {
            //Add text saying 'lives'
            Vector2 position = new Vector2(64,
                20 + 8);
            Transform2D textTransform = new Transform2D(position, 0, Vector2.One,
                Vector2.Zero, new Integer2(128, 32));
            UITextObject textObject = new UITextObject("Player Lives Text",
                ActorType.UIStaticText,
                StatusType.Drawn,
                textTransform, Color.AntiqueWhite, SpriteEffects.None, 1,
                "Lives", this.fontDictionary["menu"]);

            this.uiManager.Add(textObject);

            // Add a lives bar for the player
            Texture2D texture = this.textureDictionary["player_health"];
            Vector2 scale = new Vector2(1.75f, 1.5f);
            position = new Vector2(texture.Width,
                20);
            Transform2D transform = new Transform2D(position, 0, scale,
                Vector2.Zero, new Integer2(texture.Width, texture.Height));

            UITextureObject textureObject = new UITextureObject("Player Lives",
                ActorType.UITexture,
                StatusType.Drawn | StatusType.Update,
                transform, Color.White,
                SpriteEffects.None, 1, texture);

            textureObject.AttachController(new PlayerLivesProgressController("Player Lives Controller",
                ControllerType.UIProgress, 5, 5, this.eventDispatcher));

            this.uiManager.Add(textureObject);

            //Add text for the boss' health
            position = new Vector2((graphics.PreferredBackBufferWidth / 2.0f) + 250, 40);
            textTransform = new Transform2D(position, 0, Vector2.One,
                Vector2.Zero, new Integer2(128, 32));
            textObject = new UITextObject("Boss Health Text",
                ActorType.UIStaticText,
                StatusType.Drawn,
                textTransform, Color.AntiqueWhite, SpriteEffects.None, 1,
                "Boss", this.fontDictionary["menu"]);

            this.uiManager.Add(textObject);

            //Add a health bar for the boss
            texture = this.textureDictionary["progress_gradient_RD"];
            scale = new Vector2(1, 0.75f);
            position = new Vector2((graphics.PreferredBackBufferWidth / 4.0f) * 3, 28);
            Transform2D bossHealthTransform = new Transform2D(position, 0, scale,
                Vector2.Zero, new Integer2(texture.Width, texture.Height));
            UITextureObject bossTextureObject = new UITextureObject("Boss Health",
                ActorType.UITexture,
                StatusType.Drawn | StatusType.Update,
                bossHealthTransform, Color.PaleVioletRed,
                SpriteEffects.None, 1, texture);

            bossTextureObject.AttachController(new UIProgressController("Boss Health Controller",
                ControllerType.UIProgress, 0, 3, this.eventDispatcher));

            this.uiManager.Add(bossTextureObject);
        }
        #endregion

        #region Effects
        private void InitializeEffects()
        {
            BasicEffect basicEffect = null;
           
            #region For unlit colored primitive objects incl. simple lines and wireframe primitives
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = false;
            basicEffect.VertexColorEnabled = true;
            this.effectDictionary.Add(AppData.UnLitColoredPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For unlit textured primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = false;
            this.effectDictionary.Add(AppData.UnLitTexturedPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For lit (i.e. normals defined) textured primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);  
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = false;
            basicEffect.EnableDefaultLighting();
            basicEffect.PreferPerPixelLighting = true;
            this.effectDictionary.Add(AppData.LitTexturedPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion
        }
        #endregion

        #region Content, Update, Draw        
        protected override void LoadContent()
        {
            //moved to Initialize
            //spriteBatch = new SpriteBatch(GraphicsDevice);

//            #region Add Menu & UI
//            InitializeMenu();
//            AddMenuElements();
//            InitializeUI();
//            AddUIElements();
//            #endregion

//#if DEBUG
//            InitializeDebugTextInfo();
//#endif

        }
        protected override void UnloadContent()
        {
            //formally call garbage collection on all ContentDictionary objects to de-allocate resources from RAM
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
            this.videoDictionary.Dispose();

        }

        protected override void Update(GameTime gameTime)
        {
            //exit using new gamepad manager
            if(this.gamePadManager != null && this.gamePadManager.IsPlayerConnected(PlayerIndex.One) && this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.Back))
                this.Exit();

#if DEMO
            DoDebugToggleDemo();
            DoCameraCycle();
#endif

            base.Update(gameTime);
        }

        private void DoCameraCycle()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.F1))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }
        }

        private void DoDebugToggleDemo()
        {
            if(this.keyboardManager.IsFirstKeyPress(Keys.F5))
            {
                //toggle the boolean variables directly in the debug drawe to show CDCR surfaces
                this.collisionSkinDebugDrawer.ShowCollisionSkins = !this.collisionSkinDebugDrawer.ShowCollisionSkins;
                this.collisionSkinDebugDrawer.ShowZones = !this.collisionSkinDebugDrawer.ShowZones;
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.F6))
            {
                //toggle the boolean variables directly in the debug drawer to show the frustum culling bounding spheres
                this.collisionSkinDebugDrawer.ShowFrustumCullingSphere = !this.collisionSkinDebugDrawer.ShowFrustumCullingSphere;
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.F7))
            {
                //we can turn all debug off 
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }

        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(GoogleGreenColor);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
        #endregion
    }
}
