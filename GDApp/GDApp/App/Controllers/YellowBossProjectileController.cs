﻿using GDLibrary;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp
{
    public enum YellowBossProjectileRotationDirection : sbyte
    {
        Clockwise,
        Counterclockwise
    }

    //Used to determine whether the projectile should check if it's level with
    //the player on either the x position or the y position
    public enum YellowBossProjectileDetectionType : sbyte
    {
        NotSet,
        XPos,
        YPos,
        ZPos
    }

    public class YellowBossProjectileController : Controller
    {
        #region Fields
        private int numberOfPhases;
        private int currentPhase;
        private float travellingSpeed;
        private float[] distanceToCover;
        private float distanceTravelled;
        private int delayBetweenPhasesInMs;
        private int delayCountdownInMs;
        private YellowBossProjectileRotationDirection rotationDirection;

        //Relating to special phase after all regular phases have been completed
        private readonly float speedIncrement;
        private readonly float speedCap;
        private YellowBossProjectileDetectionType detectionType;
        private bool bHasReachedPlayer;
        private float chasePlayerSpeed;
        #endregion

        #region Properties
        public YellowBossProjectileRotationDirection RotationDirection
        {
            get
            {
                return this.rotationDirection;
            }
            set
            {
                this.rotationDirection = value;
            }
        }
        #endregion

        public YellowBossProjectileController(string id, ControllerType controllerType, int phases, float travellingSpeed,
            float[] distanceToCover, int delayBetweenPhasesInMs, YellowBossProjectileRotationDirection rotationDirection)
            : base(id, controllerType)
        {
            this.numberOfPhases = phases;
            this.currentPhase = 0;
            this.travellingSpeed = travellingSpeed;
            this.distanceToCover = distanceToCover;
            this.distanceTravelled = 0;
            this.delayBetweenPhasesInMs = delayBetweenPhasesInMs;
            this.delayCountdownInMs = 0;
            this.rotationDirection = rotationDirection;

            this.speedIncrement = 0.0001f;
            this.speedCap = 0.02f;
            this.detectionType = YellowBossProjectileDetectionType.NotSet;
            this.bHasReachedPlayer = false;
            this.chasePlayerSpeed = 0.04f;
            //distanceToCover = 13f;
            //stationaryCountdownInMs = 800;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            ProjectileCollidablePrimitiveObject parentActor = actor as ProjectileCollidablePrimitiveObject;

            if (parentActor != null)
            {
                //Check how many normal phases the controller has to go through
                if (currentPhase != numberOfPhases)
                {
                    //Go through the normal phases
                    AdvanceNormalPhase(gameTime, parentActor);
                }
                else
                {
                    //Perform the special end phase
                    AdvanceSpecialEndPhase(gameTime, parentActor);
                }
            }
        }

        private void AdvanceNormalPhase(GameTime gameTime, ProjectileCollidablePrimitiveObject parentActor)
        {
            //Check if the delay countdown is empty
                //If it is, take note of the distance the projectile has covered
                //If the distance covered exceeds the current target distance, set the delay countdown
            //If it's not, keep the projectile still
                //Decrease the timer
                //Once it runs out, advance the phase and change the proj direction

            if (delayCountdownInMs <= 0)
            {
                parentActor.MoveSpeed = travellingSpeed;
                distanceTravelled += parentActor.MoveSpeed * gameTime.ElapsedGameTime.Milliseconds;
                if (distanceTravelled >= distanceToCover[currentPhase])
                {
                    delayCountdownInMs = delayBetweenPhasesInMs;
                }
            } else
            {
                parentActor.MoveSpeed = 0;
                delayCountdownInMs -= gameTime.ElapsedGameTime.Milliseconds;
                if (delayCountdownInMs <= 0)
                {
                    parentActor.MoveDirection = RotateProjectile(parentActor.MoveDirection);
                    distanceTravelled = 0;
                    currentPhase++;
                }
            }
        }

        private void AdvanceSpecialEndPhase(GameTime gameTime, ProjectileCollidablePrimitiveObject parentActor)
        {
            //Determine the projectile's detection range if it's not set
            if (detectionType == YellowBossProjectileDetectionType.NotSet)
            {
                CalculateDetectionType(parentActor);
            }

            //Check if the projectile has not yet crossed the player
            if (!bHasReachedPlayer)
            {
                parentActor.MoveSpeed = (parentActor.MoveSpeed >= speedCap) ? parentActor.MoveSpeed : parentActor.MoveSpeed + speedIncrement;
                if (IsLevelWithPlayer(parentActor))
                {
                    parentActor.MoveSpeed = this.chasePlayerSpeed;
                    parentActor.MoveDirection = RotateProjectile(parentActor.MoveDirection);
                    bHasReachedPlayer = true;
                }
            }
        }

        private Vector3 RotateProjectile(Vector3 parentActorDrection)
        {
            if (rotationDirection == YellowBossProjectileRotationDirection.Clockwise)
            {
                return new Vector3(parentActorDrection.Y, -parentActorDrection.X, parentActorDrection.Z);
            } else if (rotationDirection == YellowBossProjectileRotationDirection.Counterclockwise)
            {
                return new Vector3(-parentActorDrection.Y, parentActorDrection.X, parentActorDrection.Z);
            }

            return new Vector3(-parentActorDrection.Y, parentActorDrection.X, parentActorDrection.Z);
        }

        private void CalculateDetectionType(ProjectileCollidablePrimitiveObject parentActor)
        {
            if (parentActor.MoveDirection == Vector3.UnitY || parentActor.MoveDirection == -Vector3.UnitY)
            {
                detectionType = YellowBossProjectileDetectionType.YPos;
            } else if (parentActor.MoveDirection == Vector3.UnitX || parentActor.MoveDirection == -Vector3.UnitX)
            {
                detectionType = YellowBossProjectileDetectionType.XPos;
            } else
            {
                detectionType = YellowBossProjectileDetectionType.YPos;
            }
        }

        private bool IsLevelWithPlayer(ProjectileCollidablePrimitiveObject parentActor)
        {
            float distanceLeeway = 0.3f;
            float distance = 0;

            PlayerCollidablePrimitiveObject player = parentActor.ObjectManager.OpaqueDrawList.Find(p => p.ID.Equals("Main Player")) as PlayerCollidablePrimitiveObject;

            if (detectionType == YellowBossProjectileDetectionType.XPos)
            {
                distance = Math.Abs(parentActor.Transform.Translation.X - player.Transform.Translation.X);
            } else if (detectionType == YellowBossProjectileDetectionType.YPos)
            {
                distance = Math.Abs(parentActor.Transform.Translation.Y - player.Transform.Translation.Y);
            }

            return distance <= distanceLeeway;
        }

        public override object GetDeepCopy()
        {
            IController clone = new YellowBossProjectileController("clone - " + ID, this.ControllerType,
                this.numberOfPhases,
                this.travellingSpeed,
                this.distanceToCover.Clone() as float[], //deep copy
                this.delayBetweenPhasesInMs,
                this.rotationDirection);

            clone.SetControllerPlayStatus(this.PlayStatusType);

            return clone;
        }

        public new object Clone()
        {
            return GetDeepCopy();
        }
    }
}
