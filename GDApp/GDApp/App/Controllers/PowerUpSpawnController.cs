﻿using GDLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDApp
{
    public class PowerUpSpawnController : Controller
    {
        #region Fields
        //Fileds that are given to the controller
        private int baseCountdownTimeInMs;
        private int highestCountdownMultiplier;
        private List<ProjectileCollidablePrimitiveObject> powerUpList;

        //Fields the controller calculates itself
        private int powerUpSpawnCountdownInMs;
        private int countdownMultiplier;
        private int lastMultiplierUsed;
        
        #endregion

        #region Properties
        #endregion


        public PowerUpSpawnController(string id, ControllerType controllerType, int baseSpawnRateInMs, int highestTimeMultiplier, int firstTimeMultiplier)
            : base(id, controllerType)
        {
            baseCountdownTimeInMs = baseSpawnRateInMs;
            highestCountdownMultiplier = highestTimeMultiplier;

            powerUpList = new List<ProjectileCollidablePrimitiveObject>(5);
            countdownMultiplier = firstTimeMultiplier;
            lastMultiplierUsed = firstTimeMultiplier;
            powerUpSpawnCountdownInMs = baseCountdownTimeInMs * countdownMultiplier;

            //ResetSpawnTimer();
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            //If the spawn countdown has reached 0, prepare to spawn a powerup
            //If it has not, decrease it using the game time
            if (powerUpSpawnCountdownInMs <= 0)
            {
                //Prepare to spawn a randomly picked powerup
                PreparePowerupSpawn();

                //Reset the spawn timer
                ResetSpawnTimer();
            } else
            {
                powerUpSpawnCountdownInMs -= gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        public void AddProjectile(ProjectileCollidablePrimitiveObject projectile)
        {
            powerUpList.Add(projectile);
        }

        private void PreparePowerupSpawn()
        {
            Random random = new Random();

            //Choose a power up to spawn
            int powerUpListIndex = random.Next(powerUpList.Count);
            ProjectileCollidablePrimitiveObject powerUpToSpawn = powerUpList[powerUpListIndex].Clone() as ProjectileCollidablePrimitiveObject;

            //Choose the y-position to spawn the powerup at
            int spawnPointYPos = random.Next(9, 18);

            //Choose where the power up comes from
            if (random.Next(1, 6) < 3)
            {
                powerUpToSpawn.Transform.TranslateTo(new Vector3(-17, spawnPointYPos, 0));
                powerUpToSpawn.MoveDirection = Vector3.UnitX;
            } else
            {
                powerUpToSpawn.Transform.TranslateTo(new Vector3(17, spawnPointYPos, 0));
                powerUpToSpawn.MoveDirection = -Vector3.UnitX;
            }
            
            EventDispatcher.Publish(new EventData(powerUpToSpawn, EventActionType.OnAddActor, EventCategoryType.SystemAdd));
        }

        private void ResetSpawnTimer()
        {
            //powerUpSpawnCountdownInMs = countdownMultiplier * 2500;
            countdownMultiplier = MathUtility.RandomExcludeNumber(lastMultiplierUsed, highestCountdownMultiplier);
            countdownMultiplier = (countdownMultiplier == 0) ? lastMultiplierUsed + 1 : countdownMultiplier;

            powerUpSpawnCountdownInMs = baseCountdownTimeInMs * countdownMultiplier;
            lastMultiplierUsed = countdownMultiplier;
        }
    }
}
