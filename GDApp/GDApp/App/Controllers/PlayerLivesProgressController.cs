﻿using GDLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp
{
    public class PlayerLivesProgressController : UIProgressController
    {

        public PlayerLivesProgressController(string id, ControllerType controllerType, int startValue, int maxValue, EventDispatcher eventDispatcher)
            : base(id, controllerType, startValue, maxValue, eventDispatcher) {

        }

        protected override void HandleWinLose()
        {
            if (this.CurrentValue == this.MaxValue)
            {

            }
            else if (this.CurrentValue == 0)
            {
                object[] additionalParameters = { "A Thing" };
                EventDispatcher.Publish(new EventData(EventActionType.OnLose, EventCategoryType.Player, additionalParameters));
            }
        }
    }
}
